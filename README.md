# TrusAuth Vendor Code for Android
This repo included manifest files.

# How to use
1. Check the AOSP source code for proper functionality.
2. Identify the necessary components and their branches in the code repository based on requirements, devices, and AOSP environment.
3. Create or modify manifest files to meet specific needs and make changes to components within the files.
4. Utilize the repo tool to synchronize the code.
